FROM node:15.11.0-alpine3.10
COPY . /app
WORKDIR /app
RUN yarn
RUN yarn run build
CMD yarn node .
